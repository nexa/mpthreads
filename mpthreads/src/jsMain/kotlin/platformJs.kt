package org.nexa.threads
class JsPlatform : Platform {
    //val v = js("navigator.userAgent")
    val v = js("process.version")
    override val name: String = "Javascript " + v.toString() + " on ARCH " + js("process.arch") + " OS " + js("process.platform")
}

actual fun getPlatform(): Platform = JsPlatform()