package org.nexa.threads

import java.util.StringJoiner
import kotlin.math.min

actual fun platformName(): String = System.getProperty("os.name") + " JVM " + System.getProperty("java.version")
