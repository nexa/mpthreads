import kotlinx.atomicfu.atomic
import org.nexa.threads.*
import kotlin.concurrent.Volatile
import kotlin.test.*
import kotlin.time.TimeSource

// DO NOT RUN ANDROID TESTS from commonTest.  Run them from androidInstrumentedTest!
class CommonTests
{
    @Test
    fun test()
    {
        val p = platformName()
        check(p.isNotEmpty())
        println("Common tests on: " + p)
    }

    @Test
    fun testThreads()
    {
        println("thread test")
        var cnt = 0
        val q = mutableListOf<Long>(0)
        val th = Thread {
            // println("thunk is running")
            cnt += 1
            q.add(cnt.toLong())
        }
        th.join()
        println(cnt)
        check(cnt == 1)
        check(q.size == 2)   // check appending to a list in the thread
    }

    @Test fun testThreadException()
    {
        var cnt = 0
        DefaultThreadExceptionHandler = {
            cnt++
        }
        Thread {
            throw Exception("test")
        }.join()
        check(cnt == 1)
        // Set it back to something that is very unhappy when an exception happens for the tests
        DefaultThreadExceptionHandler = { println(it); it.printStackTrace(); throw it }
    }

    @Test fun testRecursiveMutex()
    {
        println("testRecursiveMutex")
        val mutex = Mutex()
        var a = 0
        mutex.lock {
            mutex.lock {
                a++
            }
        }
    }
    @Test
    fun testMutex()
    {
        println("testMutex")
        val mutex = Mutex()

        var cnt = 0
        var cnt2 = 0
        val th = Thread {
            // println("thunk is running")
            for (i in 1..10000) mutex.lock {
                cnt = cnt + 1
                yield()
                cnt2 = cnt2 + 1
            }

        }
        for (i in 1..10000) mutex.lock {
            cnt = cnt + 1
            yield()
            cnt2 = cnt2 + 1
            }
        th.join()
        check(cnt == cnt2)
        check(cnt2 == 20000)

        println("test timed sync, not timing out")
        var start = millinow()
        check(mutex.timedSync(10000) {
            true
        } != null)
        var end = millinow()
        check(end-start < 10000)

        println("test timed sync, timing out")
        mutex.lock {
            val t = Thread {
                var thstart = millinow()
                check(mutex.timedSync(200) {
                    check(false, {"you should never get this mutex!"})
                } == null)
                var thend = millinow()
                check(thend-thstart >= 200, {"timeout was too short!"} )
                println("timedSync timeout completed")
            }
            println("join")
            t.join()
        }
    }


    @Test
    fun testGate()
    {
        println("test gate")
        var done = false
        val lock = Gate()
        val q = mutableListOf<Int>()
        var processed:Int = -2
        val th = Thread {
            // println("thunk is running")
            while(processed < 50) {
                lock.wlock {
                    while (q.size > 0) {
                        val s = q.removeFirst()
                        //println("Consumed: $s")
                        processed = s
                        lock.wake()
                    }
                    if (processed < 50 && !done) await()
                }
            }
            // println("thread completed")
        }

        for(i in 1..50)
        {
            lock.lock {
                q.add(i)
                lock.wake()
            }
            //println("added $i")
        }

        lock.waitfor({ processed == 50 }) {
            done = true
            lock.wake()
        }

        th.join()
        lock.finalize()
    }

    @Test
    fun testTimedGate()
    {
        val lock = Gate()

        // Test wait never given
        var start = millinow()
        val ret = lock.timedwaitfor(1500, { false }, { check(false) })  // never going to execute the block
        check(ret == null, { "ret is null!!"} )
        var end = millinow()
        check(end-start >= 500, { "elapsed time is ${end-start}."})  // At least the wait amount should have passed

        // Test lock never given
        lock.lock {
            val t = Thread {
                var threadSt = millinow()
                check(lock.timedwaitfor(500, { false }, { check(false)}) == null)
                var threadEnd = millinow()
                check(threadEnd-threadSt >= 500, {"duration was ${threadEnd-threadSt}"})  // At least the wait amount should have passed
            }
            t.join()
        }
        lock.finalize()

    }

    @Test
    fun testSynchronizerWaitLoop()
    {
        done=false
        val lock = Gate()
        val q = mutableListOf<Int>()
        var processed:Int = -2
        val th = Thread {
            //println("thunk is running")
            lock.loopwhile({!done}) {
                while (q.size > 0)
                {
                    val s = q.removeFirst()
                    //println("Consumed: $s")
                    processed = s
                    lock.wake()
                }
            }

        }

        for(i in 1..50)
        {
            lock.lock {
                q.add(i)
                lock.wake()
            }
            //println("added $i")
        }

        lock.waitfor({ processed == 50 }) {
            done = true
            lock.wake()
        }

        th.join()
    }

    @Volatile
    var done = false
    @Test
    fun testSynchronizerWaitFor()
    {
        done = false
        val lock = Gate()
        val q = mutableListOf<Int>()
        var processed:Int = -1
        val th = Thread {
            // println("thunk is running")
            while(!done)
            {
                lock.waitfor({ q.size > 0 || done }) {
                    if (done) return@waitfor
                    while(q.size > 0)
                    {
                        val s = q.removeFirst()
                        // println("Consumed: $s")
                        processed = s
                        yield()
                        lock.wake()
                    }
                }
            }
        }

        for(i in 1..50)
        {
            lock.lock {
                q.add(i)
                lock.wake()
            }
            yield()
            //println("added $i")
        }

        lock.waitfor({ processed == 50 }) {
            //println("consumed all, setting done = true")
            done = true
            lock.wake()
        }

        //println("joining thread")
        th.join()
    }



    @Test
    fun repeatTest()
    {
        for(i in 0..10)
        {
            println("Iteration $i")
            testMutex()
            println("testLock")
            testGate()
            println("testLockWaitFor")
            testSynchronizerWaitFor()
            println("testLockWaitLoop")
            testSynchronizerWaitLoop()
        }
    }

    @Test fun leakyBucketTest()
    {
        val lb = LeakyBucket(50, 10, 20)
        try {
            lb.take(51)
            check(false)
        }
        catch (e: IllegalArgumentException)  // taking more than bucket max -> infinite wait
        {
            check(true)
        }

        val start = TimeSource.Monotonic.markNow()
        lb.take(10)
        val el = start.elapsedNow().inWholeMilliseconds
        if (el > 10) println("Expected approximately no elapsed time, got $el (this fails in CI due to low performance machines)")
        lb.take(30)
        val elapsed = start.elapsedNow().inWholeMilliseconds
        check(elapsed > 1900 && elapsed < 4000, { "Expected approximately 2000 elapsed ms, got $elapsed"})  // total leaked 40, started at 20, filling 10 per sec, so 2 seconds wait

        check(lb.trytake(3000, { true }) == null)
        lb.stop()
        check(lb.trytake(3000, { true }) == true)
    }

    @Test
    fun testJobs()
    {
        val jobPool = ThreadJobPool("testpool", 2, excessiveThreads = 25)
        val count = atomic(0)
        //val countLock = Mutex()

        val SMALL_TEST = 10
        for (i in 0 until SMALL_TEST)
            jobPool.later {
                count.incrementAndGet()
            }
        while(true)
        {
            //if (countLock.lock { (count==SMALL_TEST) }) break
            if (count.value == SMALL_TEST) break
            millisleep(20U)
        }

        count.value = 0
        for (i in 0 until SMALL_TEST)
            jobPool.oneLater("testonce") {
                millisleep(50U)
                count.incrementAndGet()
            }
        // testonce should only run once because the millisleep makes them all launch
        // while the first is running
        millisleep(500U)
        check(count.value == 1)

        for (testRun in 0 until 2)
        {
            jobPool.start()
            val BIG_TEST = 40
            println("Test $BIG_TEST jobs")
            jobPool.minThreadLaunchInterval = 0  // Let the pool generate a lot of threads while we delay
            jobPool.minThreadCreateInterval = 0
            count.value = 0
            val lock = Mutex()
            val lst = mutableListOf<Pair<Int, Int>>()
            for (i in 0 until BIG_TEST)
            {
                val j = i
                jobPool.later {
                    millisleep(500UL)
                    //countLock.lock { count++ }
                    val c = count.getAndIncrement()
                    lock.lock {
                        //println("job $j count $c")
                        lst.add(Pair(j, c))
                    }
                }
            }
            var loopCount = 0
            while (true)
            {
                //if (countLock.lock { (count==BIG_TEST) }) break
                //print(count.value.toString() + " ")
                if ((loopCount and 127) == 0) println()
                if (count.value == BIG_TEST) break
                if ((loopCount and 255) == 255)
                {
                    lock.lock {
                        val items = lst.sortedBy { it.first }
                        println("items " + items.map { it.first.toString() + " " + it.second.toString() })
                    }
                }
                millisleep(30U)
                loopCount++
            }

            // because the default excessive thread handler never allows additional
            check(jobPool.allThreads.size <= jobPool.excessiveThreads)
            //println("DONE: Job threads: ${jobPool.allThreads.size}")
            jobPool.stop()
            println("Stop completed")
        }
    }

    @Test
    fun testReadme()
    {
        // 1
        val th = Thread {
            println("running in a different thread")
        }
        val th1 = Thread("threadName") {
            println("running in a different thread")
        }

        // 2
        val th2 = Thread { millisleep(1000U) }
        th.join()

        //val th3 = Thread { millisleep(100000U) }
        //th.join(1000)  // wait for a second; if the thread hasn't completed yet, then throw an exception


        //3 Synchronize/Atomize access to data
        val mutex = Mutex()
        var a = 1
        var b = 2

        val c = mutex.lock {
            a = a+b
            b = b+a
            a+b
        }
        mutex.synchronized {  // same as lock, but follows Java naming
        }

        val d = mutex.ifavailable {
        }
        if (d == null)  // lock was taken, your block was NOT executed
        {}

        val e = mutex.trylock { }  // more traditional name for ifavailable

        val f = mutex.timedlock(1000) {}
        if (f == null)  // timeout, your block was NOT executed
        {}


        //4 Synchronize threads
        var done = false
        val gate = Gate()
        val q = mutableListOf<Int>()
        Thread {
            gate.loopwhile({!done})  // loop, waiting for some other thread to wake me (gate is locked, except when this thread is waiting)
            {
                while (q.size > 0)  // Always consume everything you can, because there's no guarantee that 1 call to the wake function == 1 wake up
                {
                    val s = q.removeFirst()
                    println(s.toString())
                }
            }
        }

        for(i in 1..50)
        {
            gate.synchronized {
                q.add(i)
                gate.wake()
            }
        }
        // ok all done so wake up
        gate.synchronized {
            done=true
            gate.wake()
        }
    }
}
