package org.nexa.threads

/** This function converts a function that takes an asynchronously executed callback into a blocking function
    for example,

fun ask(question:String, answer: (String)->Unit)
{
   ... makes an async request and calls answer when a response appears ...
}

fun test()
{
    val response = blocking(10000) { ask("What is the question with the answer 42?", it)}
    if (response == null)
}
 */
fun<T> blocking(timeoutMs: Long, call:((T)->Unit)->Unit):T?
{
    val g = Gate()
    var done = false
    var ret:T? = null
    call { it ->
        done = true
        ret = it
        g.wake()
    }
    g.timedwaitfor(timeoutMs, { done == true }) {}
    g.finalize()
    return ret
}

fun<T> blocking(call:((T)->Unit)->Unit):T?
{
    val g = Gate()
    var done = false
    var ret:T? = null
    call { it ->
        done = true
        ret = it
        g.wake()
    }
    g.waitfor({ done == true }) {}
    g.finalize()
    return ret
}


/** This function converts a function that takes an asynchronously executed callback into a blocking function
for example,

fun ask(question:String, answer: (String)->Unit)
{
... makes an async request and calls answer when a response appears ...
}

fun test()
{
val response = blocking(10000) { ask("What is the question with the answer 42?", it)}
if (response == null)
}
 */
fun<T0, T1> blocking2(timeoutMs: Long, call:((T0?, T1?)->Unit)->Unit):Pair<T0?, T1?>
{
    val g = Gate()
    var done = false
    var ret0:T0? = null
    var ret1:T1? = null
    call { it0, it1 ->
        ret0 = it0
        ret1 = it1
        done = true
        g.wake()
    }
    g.timedwaitfor(timeoutMs, { done == true }) {}
    g.finalize()
    return Pair(ret0, ret1)
}

fun<T0, T1> blocking2(call:((T0?, T1?)->Unit)->Unit):Pair<T0?, T1?>
{
    val g = Gate()
    var done = false
    var ret0:T0? = null
    var ret1:T1? = null
    call { it0, it1 ->
        ret0 = it0
        ret1 = it1
        done = true
        g.wake()
    }
    g.waitfor({ done == true }) {}
    g.finalize()
    return Pair(ret0, ret1)
}

/** This function converts a function that takes an asynchronously executed callback into a blocking function
for example,

fun ask(question:String, answer: (String)->Unit)
{
... makes an async request and calls answer when a response appears ...
}

fun test()
{
val response = blocking(10000) { ask("What is the question with the answer 42?", it)}
if (response == null)
}
 */
fun<T0, T1, T2> blocking3(timeoutMs: Long, call:((T0?, T1?, T2?)->Unit)->Unit):Triple<T0?, T1?, T2?>
{
    val g = Gate()
    var done = false
    var ret0:T0? = null
    var ret1:T1? = null
    var ret2:T2? = null
    call { it0, it1, it2 ->
        ret0 = it0
        ret1 = it1
        ret2 = it2
        done = true
        g.wake()
    }
    g.timedwaitfor(timeoutMs, { done == true }) {}
    g.finalize()
    return Triple(ret0, ret1, ret2)
}

fun<T0, T1, T2> blocking3(call:((T0?, T1?, T2?)->Unit)->Unit):Triple<T0?, T1?, T2?>
{
    val g = Gate()
    var done = false
    var ret0:T0? = null
    var ret1:T1? = null
    var ret2:T2? = null
    call { it0, it1, it2 ->
        ret0 = it0
        ret1 = it1
        ret2 = it2
        done = true
        g.wake()
    }
    g.waitfor({ done == true }) {}
    g.finalize()
    return Triple(ret0, ret1, ret2)
}
