package org.nexa.threads
import kotlinx.atomicfu.atomic
import kotlin.concurrent.Volatile
import kotlin.math.max

//private val LogIt = GetLog("BU.mpthreads.job")
var JobDebug = false

var JobPoolExcessiveThreadHandler: ((ThreadJobPool)->Boolean) = { it -> false }
var JobExceptionHandler:(Exception) -> Unit = {}

class ThreadJob(val name: String?=null, val thunk: ()->Unit)
{
    var launchLocation:String = ""  // For debugging who is launching a job
    @Volatile var created:Long  = 0
    @Volatile var launched:Long = 0
    @Volatile var failed: Exception? = null

    fun reset()
    {
        if(JobDebug) launchLocation = src(10)
        created = millinow()
    }

    fun go()
    {
        launched = millinow()
        try { thunk() }
        catch(e:Exception)
        {
            failed = e
        }
    }
}

class JobStats
{
    data class Snapshot(var launches: Long=0, var launchLatencyMs: Double=0.0, var executionTimeMs: Double=0.0, var maxLaunchLatencyMs: Long=0, var maxExecutionTimeMs: Long=0)
    {
        fun update(launchLatency: Long, executionTime:Long)
        {
            val ol = launches
            launches+=1
            launchLatencyMs = ((ol*launchLatencyMs) + launchLatency.toDouble())/launches
            executionTimeMs = ((ol*executionTimeMs) + executionTime.toDouble())/launches
            maxExecutionTimeMs = max(maxExecutionTimeMs, executionTime)
            maxLaunchLatencyMs = max(maxLaunchLatencyMs, launchLatency)
        }
    }

    val alltime = Snapshot()
    val interval =  Snapshot()

    fun reset()
    {
        interval.launches=0
        interval.executionTimeMs=0.0
        interval.launchLatencyMs=0.0
        interval.maxLaunchLatencyMs=0
        interval.maxExecutionTimeMs=0
    }

    fun update(launchLatency: Long, executionTime:Long)
    {
        if (interval.launches>=100) reset()
        alltime.update(launchLatency, executionTime)
        interval.update(launchLatency,executionTime)
    }
}


class ThreadJobPool(val name: String, val minThreads:Int, start:Boolean=true, var excessiveThreads:Int = 20,var minThreadCreateInterval:Int = 500, var minThreadLaunchInterval:Int = 25)
{
    var excessiveThreadHandler = JobPoolExcessiveThreadHandler
    var jobStuckCutoff = 2000
    val allThreads = mutableListOf<iThread>()
    //val availThreads = mutableListOf<iThread>()
    var availableThreads = 0
    enum class ProcessingState
    {
        STARTED, PAUSING, PAUSED, STOPPING, STOPPED
    }
    @Volatile var poolState = ProcessingState.STOPPED
    val wakey = Gate()
    // Note the linkedMap appear to be both inefficient on iOS and sometimes fails to enqueue
    //val jobs = linkedMapOf<String,ThreadJob>()
    val jobs = mutableListOf<ThreadJob>()
    val jobLookup = mutableMapOf<String, ThreadJob>()
    @Volatile var lastLaunch: Long = millinow()
    @Volatile var lastCreate: Long = millinow()
    val jobCount = atomic(0)

    // Diagnostics
    val jobStats = mutableMapOf<String, JobStats>()
    //val runningJobs = mutableMapOf<String, ThreadJob>()

    init {
        if (start) start()
    }
    // Only for ThreadJob to call when it is complete
    internal fun finishJob(job:ThreadJob)
    {
        val end = millinow()
        val name = job.name
        if (name != null)
        {
            jobLookup.remove(name)
            val js = jobStats.getOrPut(name, { JobStats() })
            // LogIt.info("job created: ${job.created}")
            js.update(job.launched - job.created, end - job.launched)
        }
    }

    fun oneLater(jobName: String,job: ()->Unit)
    {
        wakey.lock {
            if (!jobLookup.containsKey(jobName))
                _later(true, jobName, job)
        }
    }

    fun later(jobName: String?=null, job: ()->Unit)
    {
        wakey.lock {
            _later(false, jobName, job)
        }
    }


    protected fun _later(one: Boolean, jobName: String?=null, job: ()->Unit)
    {
        // wakey must be locked

        val jc = jobCount
        jobCount.getAndIncrement()
        val name = if (one)
        {
            // if (jobName.length > 128) throw IllegalArgumentException("Job name too long")
            jobName ?: throw IllegalArgumentException("Single jobs must be named")
        }
        else
        {
            null // (jobName ?: "job") + "_" + jobCount.toString()
        }
        val tj = ThreadJob(name, job)
        val needToWake = jobs.isEmpty()
        name?.let { jobLookup[it] = tj }
        jobs.add(tj)
        // println("Q: $name, enqueued: ${jobName}")
        tj.reset()
        val now = tj.created
        if ((availableThreads < jobs.size) && (now >= minThreadLaunchInterval + lastLaunch)
            && (now >= minThreadCreateInterval + lastCreate)
        ) expand(now)
        if (needToWake) wakey.wake()
    }

    protected fun expand(now: Long)
    {
        val create = if (allThreads.size < excessiveThreads) true else excessiveThreadHandler(this)
        if (create)
        {
            val t = org.nexa.threads.Thread(name + allThreads.size.toString(), ::go)
            allThreads.add(t)
            lastCreate = now
        }
    }

    fun start()
    {
        // org.nexa.libnexakotlin.assert(poolState == ProcessingState.STOPPED)
        poolState = ProcessingState.STARTED
        val now = millinow()
        while(allThreads.size < minThreads) expand(now)
    }

    fun pause()
    {
        poolState = ProcessingState.PAUSING
        wakey.wake()
        for (a in allThreads) a.join()
        allThreads.clear()
        poolState = ProcessingState.PAUSED
    }

    fun stop()
    {
        wakey.lock {
            poolState = ProcessingState.STOPPING
            wakey.wake()
        }
        for (a in allThreads) a.join()
        allThreads.clear()
        poolState = ProcessingState.STOPPED
    }

    /** Returns a list of probably stuck jobs, sorted by longest running first */
    fun stuck(): List<Pair<String,ThreadJob>>
    {
        val now = millinow()
        val ret = mutableListOf<Pair<String,ThreadJob>>()
        for(i in jobLookup)
        {
            if (i.value.launched > 0)
            {
                if (now - i.value.launched >= jobStuckCutoff) ret.add(Pair(i.key, i.value))
            }
        }
        ret.sortByDescending { i -> i.second.launched }
        return ret
    }

    fun logStats():String
    {
        val stats = jobStats.toList().sortedBy { it.second.alltime.maxExecutionTimeMs }
        val s = StringBuilder()
        for (j in stats)
        {
            s.append("Job: ${j.first} calls: ${j.second.alltime.launches}  exec: ${j.second.alltime.maxExecutionTimeMs} (avg: ${j.second.alltime.executionTimeMs})  launch: ${j.second.alltime.maxLaunchLatencyMs} (avg: ${j.second.alltime.launchLatencyMs})\n")
            s.append("    interval    calls: ${j.second.interval.launches}  exec: ${j.second.interval.maxExecutionTimeMs} (avg: ${j.second.interval.executionTimeMs})  launch: ${j.second.interval.maxLaunchLatencyMs} (avg: ${j.second.interval.launchLatencyMs})\n")
        }
        return("Job pool stats: $name  threads: ${allThreads.size}\n${s}")
    }

    fun go()
    {
        setThreadName("Job_${name}_${allThreads.size}")
        while ((poolState != ProcessingState.STOPPING) && (poolState != ProcessingState.STOPPED))
        {
            var job: ThreadJob? = null
            // var jname: String? = null
            availableThreads++
            wakey.waitfor({ jobs.isNotEmpty() || (poolState != ProcessingState.STARTED) }) {
                // println("job thread woke poolState is $poolState")
                if (poolState == ProcessingState.STARTED)
                {
                    // a job that was added is not always being executed
                    val iter = jobs.iterator()
                    if (iter.hasNext())
                    {
                        val entry = iter.next()
                        job = entry //.value
                        // jname = entry.key
                        iter.remove()
                    }
                }
            }
            availableThreads--
            try
            {
                val j = job
                if (j != null)
                {
                    val start = millinow()
                    lastLaunch = start
                    // LogIt.info(sourceLoc() +": Job pool $name, launching job ${j.name?:"unamed"} with ${jobs.size} waiting")
                    //runningJobs.add(j)
                    //jname?.let { runningJobs[it] = j }
                    j.go()
                    val end = millinow()
                    finishJob(j)
                    //jname?.let { runningJobs.remove(jname) }
                    // LogIt.info(sourceLoc() +": Job pool $name, completed job ${j.name?:"unamed"} in ${end - start}ms")
                    /* STUCK MONITOR
                    val stuckjobs = stuck()
                    if (stuckjobs.size>0)
                    {
                        val s = StringBuilder()
                        s.append("\n")
                        stuckjobs.forEach {
                            s.append("  Job ${it.name}: launched ${end - it.launched}ms ago\n")
                        }
                        LogIt.info(sourceLoc() + ": Job pool $name: ${stuckjobs.size} STUCK jobs!! ${s}")
                    }
                    logStats()
                     */
                }
            }
            catch(e: Exception)
            {
                JobExceptionHandler(e)
            }
        }
        availableThreads--
        // println("Job thread completed")
    }
}

