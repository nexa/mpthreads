import java.util.Properties
import java.io.FileInputStream

plugins {
    kotlin("multiplatform")
    id("com.android.library")
    id("maven-publish")
    id("org.jetbrains.dokka") version "1.9.20"
}

// Host determination
val LINUX = System.getProperty("os.name").lowercase().contains("linux")
val MAC = System.getProperty("os.name").lowercase().contains("mac")
val MSWIN = System.getProperty("os.name").lowercase().contains("windows")

if (MAC) println("Host is a MAC, MacOS and iOS targets are enabled")
else println("MacOS and iOS targets are disabled")

if (LINUX) println("Host is LINUX, Android, JVM, and LinuxNative targets are enabled")
else println("Linux target is disabled")

// grab local properties into an object
val prop = Properties().apply {
    load(FileInputStream(File(rootProject.rootDir, "local.properties")))
}

fun mustExist(dir:String):File
{
    val f = file(dir)
    if (!f.exists()) throw Exception("missing $f")
    return f
}

kotlin {
    // targetHierarchy.default()
    kotlin.applyDefaultHierarchyTemplate()
    task("testClasses")

    androidTarget {
        compilations.all {
            kotlinOptions {
                jvmTarget = "17"
            }
        }
        publishLibraryVariants("release","debug")
    }

    jvm {
        compilations.all {
            kotlinOptions {
                jvmTarget = "17"
            }
        }
    }

    listOf(
        iosX64(),
        iosArm64(),
        iosSimulatorArm64()
    ).forEach {
        it.binaries.framework {
            baseName = "shared"
        }
    }
    macosX64 {
    }
    macosArm64 {
    }
    // MS windows
    mingwX64 {
    }

    linuxX64 {
    }


    sourceSets {
        val commonMain by getting {
            dependencies {
                //put your multiplatform dependencies here
                implementation("org.jetbrains.kotlinx:atomicfu:0.25.0")  // https://github.com/Kotlin/kotlinx-atomicfu
                implementation(kotlin("stdlib-common"))
                implementation("org.jetbrains.kotlinx:kotlinx-datetime:0.6.0") // https://github.com/Kotlin/kotlinx-datetime/tags
            }
        }
        val commonTest by getting {
            dependencies {
                implementation(kotlin("test"))
            }
        }

        create("commonJvm") {
            kotlin.srcDir(mustExist("src/commonJvm/kotlin"))
            dependsOn(sourceSets.named("commonMain").get())
        }

        val androidMain by getting {
            // Android studio's syntax highlighting compiler can't handle additional srcdirs like this (although the compile works)
            // for now, use symlinks.
            // Note: mswindows cannot use symlinks
            dependsOn(sourceSets.named("commonJvm").get())
            dependsOn(sourceSets.named("commonMain").get())
            dependencies {
                implementation(kotlin("stdlib-jdk8"))
            }
        }
        
        val jvmMain by getting {
            dependsOn(sourceSets.named("commonJvm").get())
        }

        val androidInstrumentedTest by getting {
            dependencies {
                implementation(kotlin("test-junit"))
                implementation("androidx.test:core:1.6.1")
                implementation("androidx.test:core-ktx:1.6.1")
                implementation("androidx.test.ext:junit:1.2.1")
                implementation("androidx.test.ext:junit-ktx:1.2.1")

                // implementation("androidx.test.espresso:espresso-core:3.6.1")
                //implementation("org.jetbrains.kotlinx:kotlinx-coroutines-test:$coroutines")
                //implementation("org.jetbrains.kotlinx:kotlinx-coroutines-android:$coroutines")
            }
        }
    }

    // Stop publication duplication
    val publicationsFromLinuxOnly =
        listOf(jvm(), androidTarget(), mingwX64(), linuxX64()).map { println("linux only ${it.name}"); it.name } + "kotlinMultiplatform" + "androidDebug" + "androidRelease"
    publishing {
        publications {
            matching { val name = it.name; publicationsFromLinuxOnly.filter { it in name }.size > 0 }.all {
                tasks.withType<AbstractPublishToMaven>()
                    .matching {
                        val pub = it.publication
                        if (pub != null) {
                            pub.name in publicationsFromLinuxOnly
                        } else false
                    }
                    .configureEach { onlyIf { LINUX } }
            }
        }
    }
}

android {
    namespace = "org.nexa.threads"
    ndkVersion = "27.0.11902837"  // Pin the NDK because this version is installed in the automated CI docker image
    compileSdk = 35
    defaultConfig {
        targetSdk = 35
        minSdk = 26
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"

        externalNativeBuild {
            cmake {
                targets.add("mpthreads")
            }
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }
    externalNativeBuild {
        cmake {
            path("src/androidMain/cpp/CMakeLists.txt")
        }
    }
}


group="org.nexa"
version="0.3.3"

publishing {
        repositories {
        maven {
            // Project ID number is shown just below the project name in the project's home screen
            url = uri("https://gitlab.com/api/v4/projects/48544966/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                name = "Deploy-Token"
                value = prop.getProperty("mpThreadsDeployToken")
            }
            authentication {
                create<HttpHeaderAuthentication>("header")
            }
        }
    }
}