# Kotlin Multiplatform Threads

## Include in your build

In settings.gradle.kts:
```kotlin
// by putting your dependency repos in settings.gradle.kts, they work for all subprojects
dependencyResolutionManagement {
    repositories {
        maven {
            url = uri("https://gitlab.com/api/v4/projects/48544966/packages/maven")
        }  // mpthreads
        // other dependencies you might have
    }
}
```

In build.gradle.kts:
```kotlin
// Put this somewhere near the top of your code
val mpThreadsVersion = "0.2.1" // Check the latest version here: https://gitlab.com/nexa/mpthreads/-/packages

// this demonstrates where to put the line for gradle newbies; the only line you should actually add is
// the "implementation(...)" below.
kotlin {
    ...
    sourceSets {
        ...
        val commonMain by getting {
            dependencies {
                ...
                implementation("org.nexa:mpthreads:$mpThreadsVersion")  
            }
        }
    }
}
```


## API

```kotlin
import org.nexa.threads.*
```

### Create a thread:

```kotlin
val th = Thread {
    println("running in a different thread")
}
val th1 = Thread("threadName") {
    println("running in a different thread")
}

```

### Wait for a thread to complete
```kotlin
val th = Thread { println("running in a different thread") }
th.join()
```

### Synchronize/Atomize access to data
```kotlin
val mutex = Mutex()
var a = 1
var b = 2

val c = mutex.lock {
    a = a+b
    b = b+a
    a+b
}
mutex.synchronized {  // same as lock, but follows Java naming
}

val d = mutex.ifavailable {
}
if (d == null)  // lock was taken, your block was NOT executed
{}

val e = mutex.trylock { }  // more traditional name for ifavailable

val f = mutex.timedlock(1000) {}
if (f == null)  // timeout, your block was NOT executed
{}
```

### Synchronize threads

The traditional thread conditions are a bit tricky.  For example, if you ever release the mutex while not sleeping, the wake event could
happen at that exact moment and so the thread misses it.  I've attempted to avoid many of those pitfalls in this API.

```kotlin
var done = false
val gate = Gate()
val q = mutableListOf<Int>()
Thread {
    gate.loopwhile({!done})  // loop, waiting for some other thread to wake me (gate is locked, except when this thread is waiting)
    {
        while (q.size > 0)  // Always consume everything you can, because there's no guarantee that 1 call to the wake function == 1 wake up
        {
            val s = q.removeFirst()
            println(s.toString())
        }
    }
}

for(i in 1..50)
{
    gate.synchronized {
        q.add(i)
        gate.wake()
    }
}
// ok all done so wake up
gate.synchronized {
    done=true
    gate.wake()
}
```


## Implementation

Android and Jvm use Java threads.  Linux, Ios, MacOS, and MsWindows use POSIX threads.

### Warnings

As in any multiplatform project, you'll get false errors since your Android Studio host cannot cross build for every single target.  Ignore them and focus only on the targets your host supports.

## Package Maintainer Notes

### Build all libraries

```bash
./gradlew assemble
```

Now find the libraries:

```bash
find . -name "mpthread*" -print
```
### Publish
Add your deploy token to local.properties as:
mpThreadsDeployToken=xxxxxxx
```
./gradlew publish
```

### To publish locally
```
./gradlew publishToMavenLocal
```
#### Check it
```bash
cd ~/.m2
find . -name "mpthread*" -print
```